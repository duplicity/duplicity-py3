#!/bin/bash
# -*- Mode:Shell; indent-tabs-mode:nil; tab-width -*-
#
# Copyright 2022 Kenneth Loafman <kenneth@loafman.com>
#
# This file is part of duplicity.
#
# Duplicity is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# Duplicity is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with duplicity; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

set -e

if [ "$1" != "" ]; then
    REL=$1
else
    echo "usage: $0 version"
    exit 1
fi

echo "Prepping for release ${REL}"

while true; do
    read -n 1 -p "Is ${REL} the correct version (y/n)?" yn
    case $yn in
        [Yy]* ) echo; break;;
        [Nn]* ) echo; exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

if [ `uname` == "Darwin" ]; then
    SED=gsed
else
    SED=sed
fi

set -v

# put in correct version for Launchpad
${SED} -i s/${REL}dev/${REL}/g setup.py
git commit -a -m"chg:pkg: Prep for ${REL}"

# add release tag and push it
git tag -f rel.${REL}
git push --tags -f origin main -o ci.skip

# make changelog and move tag to include it
tools/makechangelog
git commit -m"chg:pkg: Update changelog." -m"[skip tests]" CHANGELOG.md
git push
git tag -f rel.${REL}
git push --tags -f origin main

# update all repos
git push mirror main
git push alpha main -o ci.skip

# make and sign the release
./setup.py sdist --dist-dir=.
gpg --use-agent -b duplicity-${REL}.tar.gz

# move releases to duplicity-releases
mv duplicity-${REL}.* ../duplicity-releases
